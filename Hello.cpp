#include <afxwin.h>
#include <math.h>
#include "Hello.h"

CMyApp myApp;

BOOL CMyApp::InitInstance() {
	m_pMainWnd = new CMainWindow;

	m_pMainWnd->ShowWindow(m_nCmdShow);
	m_pMainWnd->UpdateWindow();
	return TRUE;
}

BEGIN_MESSAGE_MAP(CMainWindow, CFrameWnd)
	ON_WM_PAINT()
END_MESSAGE_MAP()

CMainWindow::CMainWindow() {
	Create(nullptr,
		_T("Loz's Fart Joy Twiddle"),
		WS_OVERLAPPEDWINDOW | WS_EX_CLIENTEDGE | WS_EX_LEFT,
		CRect(0, 0, 1200, 600));

}

void CMainWindow::OnPaint() {

	const uint32_t SEGMENTS = 10;
	const float PI = 3.1415926;

	CPaintDC dc(this);

	CRect rect;
	GetClientRect(&rect);
	// dc.SetMapMode(MM_ANISOTROPIC);
	// dc.SetWindowExt(rect.Width(), rect.Height());
	// dc.SetViewportExt(1000, 500);

	int nW = rect.Width();
	int nH = rect.Height();

	CPen pen(PS_SOLID, 0, RGB(50, 255, 10));
	dc.SelectObject(&pen);

	CBrush brush1(HS_DIAGCROSS, RGB(30, 160, 222));

	dc.MoveTo((rect.left + 10), (rect.top + 10)); // Pen has to be SOLID and 0 width
	dc.LineTo((rect.right - 10), (rect.bottom - 10));
	dc.MoveTo((rect.right - 10), (rect.top + 10));
	dc.LineTo((rect.left + 10), (rect.bottom - 10));

	//dc.SetBkMode(TRANSPARENT);
	//dc.SetROP2(R2_MERGEPENNOT);
	dc.SelectObject(&brush1);
	dc.SetBkColor(RGB(234, 76, 86));
	dc.Ellipse((rect.left + 50), (rect.top + 50),
		(rect.right - 50), (rect.bottom - 50));

	CFont font;
	font.CreatePointFont(720, _T("Comic Sans"));
	dc.SelectObject(&font);

	// If I don't make Bk transparent the bk obliterates everything
	// Shadow Effect
	dc.SetBkMode(TRANSPARENT);

	CString str1 = _T("Some Text");


	rect.OffsetRect(16, 16);
	dc.SetTextColor(RGB(192, 192, 192));
	dc.DrawText(str1, &rect, DT_SINGLELINE | DT_CENTER | DT_VCENTER);
	rect.OffsetRect(-10, -10);
	dc.SetTextColor(RGB(192, 240, 150));
	dc.DrawText(str1, &rect, DT_SINGLELINE | DT_CENTER | DT_VCENTER);

	// Text exactly spaced from each side and in the middle
	CSize size = dc.GetTextExtent(str1);
	dc.SetTextJustification(nW - size.cx, 1);
	dc.TextOutA(0, (nH / 2) - (size.cy / 2), str1);

	dc.SetTextJustification(0, 0);
	dc.SetTextColor(RGB(30, 130, 231));

	for (int i = 0; i < 3600; i += 150) {
		LOGFONT lf;
		::ZeroMemory(&lf, sizeof(lf));
		lf.lfHeight = 120;
		lf.lfWeight = FW_BOLD;
		lf.lfEscapement = i;
		lf.lfOrientation = 3600 - i;
		::lstrcpy(lf.lfFaceName, _T("Arial"));
		CFont font;
		font.CreatePointFontIndirect(&lf);
		CFont* pOldFont = dc.SelectObject(&font);
		dc.TextOut(nW / 2, nH / 2, CString(_T(" Hello, MFC")));
		dc.SelectObject(pOldFont);
	}

	dc.SetMapMode(MM_LOENGLISH);
	dc.SetTextAlign(TA_CENTER | TA_BOTTOM);
	dc.SetBkMode(TRANSPARENT);
	//
	// Draw the body of the ruler.
	//
	CBrush brush2(RGB(255, 255, 0));
	CBrush* pOldBrush = dc.SelectObject(&brush2);
	dc.Rectangle(100, -100, 1300, -200);
	dc.SelectObject(pOldBrush);
	//
	// Draw the tick marks and labels.
	//
	int i = 0;
	for (i = 125; i < 1300; i += 25) {
		dc.MoveTo(i, -192);
		dc.LineTo(i, -200);
	}
	for (i = 150; i < 1300; i += 50) {
		dc.MoveTo(i, -184);
		dc.LineTo(i, -200);
	}
	for (i = 200; i < 1300; i += 100) {
		dc.MoveTo(i, -175);
		dc.LineTo(i, -200);
		CString string;
		string.Format(_T("%d"), (i / 100) - 1);
		dc.TextOut(i, -175, string);
	}
	//CPoint points[SEGMENTS];

	//for (size_t i = 0; i < SEGMENTS; i++)
	//{
	//	points[i].x = (i * nW) / SEGMENTS;
	//	points[i].y = (int)(nH / 2) * (1 - (sin((2 * PI * i) / SEGMENTS)));
	//}
	//dc.Polyline(points, SEGMENTS);



}